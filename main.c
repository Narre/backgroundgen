//  BackgroundGen
//  Copyleft 🄯 2020-2021 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "version.h"

#define LX (position_x-1)
#define LY position_y
#define RX (position_x+1)
#define RY position_y
#define TX position_x
#define TY (position_y-1)
#define BX position_x
#define BY (position_y+1)

typedef enum {
	LEFT,
	RIGHT,
	TOP,
	BOTTOM
} direction;

int width = 0;
int height = 0;
int width_factor = 0;
int height_factor = 0;
int decoration_factor = 0;

int width_redefines = 0;
int height_redefines = 0;
int output_redefines = 0;
unsigned char fnout = 0;

int no_of_layers = 0;
int **layers;
float *layer_speed;
char **layer_name;
int *layer_zpos;
char *output_filename;
char *nout;
int *solid;
int *wrap;
int *max_height;
int *centre_22;
int *decor22;
int *decor33;
int *flip;

char *author;

unsigned char save_as_level = 0;

int hole_frequency_factor;
int max_hole_width;
int min_hole_width;
int disable_shallow;
int disable_single;

char ***tilemap;

char *strmrg(const char *restrict s1, const char *restrict s2) {
	char *s3;
	s3 = (char *)malloc((strlen(s1) + strlen(s2) + 1) * sizeof(char));
	register unsigned int i;
	for(i = 0; i < strlen(s1); ++i) {
		s3[i] = s1[i];
	}
	for(i = 0; i < strlen(s2); ++i) {
		s3[i + strlen(s1)] = s2[i];
	}
	s3[strlen(s1) + strlen(s2)] = '\0';
	return s3;
}

char is_solid(int layer) {
	if(solid[layer]) return 't';
	return 'f';
}

unsigned char strc(const char *restrict s1, const char *restrict s2, unsigned int lim) {
	if(lim > strlen(s1) || lim > strlen(s2)) return 0;
	for(register unsigned int i = 0; i < lim; ++i) if(s1[i] != s2[i]) return 0;
	return 1;
}

unsigned char parse(char *file) {
	register int i;
	FILE *preset = fopen(file, "r");

	if(preset == NULL) {
		fprintf(stderr, "Preset \"%s\" could not be found.\n", file);
		return 1;
	}

	int version = -1;
	fscanf(preset, "%i", &version);
	if(version != PRESET_VERSION) {
		fprintf(stderr, "The preset is not compatible with this version of BackgroundGen.\n");
		fclose(preset);
		return 1;
	}

	char *name;

	fscanf(preset, "%ms %ms %i %i %i %i %i %i %i %i %i %i %i", &name, &author, &width, &height, &width_factor, &height_factor, &decoration_factor, &hole_frequency_factor, &max_hole_width, &min_hole_width, &disable_shallow, &disable_single, &no_of_layers);
	printf("Loaded preset \"%s\" by \"%s\"\n", name, author);
	free(name);

	layers = malloc(no_of_layers * sizeof(int *));
	for(i = 0; i < no_of_layers; ++i) {
		layers[i] = malloc(103 * sizeof(int));
	}
	layer_speed = (float *)malloc(no_of_layers * sizeof(float));
	layer_name = (char **)malloc(no_of_layers * sizeof(char *));
	layer_zpos = (int *)malloc(no_of_layers * sizeof(int));
	solid = (int *)malloc(no_of_layers * sizeof(int));
	wrap = (int *)malloc(no_of_layers * sizeof(int));
	max_height = (int *)malloc(no_of_layers * sizeof(int));
	centre_22 = (int *)malloc(no_of_layers * sizeof(int));
	decor22 = (int *)malloc(no_of_layers * sizeof(int));
	decor33 = (int *)malloc(no_of_layers * sizeof(int));
	flip = (int *)malloc(no_of_layers * sizeof(int));

	for(i = 0; i < no_of_layers; ++i) {
		fscanf(preset, "%ms %f %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i", &layer_name[i], &layer_speed[i], &layer_zpos[i], &solid[i], &wrap[i], &max_height[i], &centre_22[i], &decor22[i], &decor33[i], &flip[i], &layers[i][3], &layers[i][0], &layers[i][4], &layers[i][1], &layers[i][9], &layers[i][2], &layers[i][5], &layers[i][10], &layers[i][6], &layers[i][8], &layers[i][7], &layers[i][11], &layers[i][12], &layers[i][13], &layers[i][14], &layers[i][15]);
		if(flip[i]) printf("[WARNING] This version of BackgroundGen does not yet support tilemap flipping. Tilemap \"%s\" will not be flipped.\n", layer_name[i]);
		if(max_height[i] > height - 4) max_height[i] = height - 4;
		if(wrap[i]) {
			fscanf(preset, "%i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i", &layers[i][16], &layers[i][17], &layers[i][18], &layers[i][19], &layers[i][20], &layers[i][21], &layers[i][22], &layers[i][62], &layers[i][63], &layers[i][23], &layers[i][24], &layers[i][25], &layers[i][26], &layers[i][27], &layers[i][28], &layers[i][29], &layers[i][30], &layers[i][31], &layers[i][32], &layers[i][64], &layers[i][65], &layers[i][33], &layers[i][34], &layers[i][35], &layers[i][36], &layers[i][37], &layers[i][38], &layers[i][39], &layers[i][40], &layers[i][41], &layers[i][49], &layers[i][42], &layers[i][43], &layers[i][44], &layers[i][45], &layers[i][46], &layers[i][47], &layers[i][48]);
		}
		if(decor22[i]) fscanf(preset, "%i %i %i %i", &layers[i][50], &layers[i][51], &layers[i][52], &layers[i][53]);
		if(decor33[i]) fscanf(preset, "%i %i %i %i %i %i %i %i %i", &layers[i][88], &layers[i][89], &layers[i][90], &layers[i][91], &layers[i][92], &layers[i][93], &layers[i][94], &layers[i][95], &layers[i][96]);
		if(wrap[i] > (unsigned char)1) fscanf(preset, "%i %i %i %i %i %i %i %i", &layers[i][54], &layers[i][55], &layers[i][56], &layers[i][57], &layers[i][58], &layers[i][59], &layers[i][60], &layers[i][61]);
		if(centre_22[i]) {
			//regular 2x2 centre (middle, top, left+right, bottom_unused)
			fscanf(preset, "%i %i %i %i %i %i %i %i %i %i %i %i", &layers[i][66], &layers[i][67], &layers[i][68], &layers[i][69], &layers[i][70], &layers[i][71], &layers[i][72], &layers[i][73], &layers[i][74], &layers[i][75], &layers[i][76], &layers[i][77]);
			if(wrap[i]) fscanf(preset, "%i %i %i %i %i %i %i %i", &layers[i][78], &layers[i][79], &layers[i][80], &layers[i][81], &layers[i][82], &layers[i][83], &layers[i][84], &layers[i][85]);
			if(wrap[i] > 1) fscanf(preset, "%i %i", &layers[i][86], &layers[i][87]);
		}
		if(wrap[i] > (unsigned char)2) fscanf(preset, "%i %i %i %i %i %i", &layers[i][97], &layers[i][98], &layers[i][99], &layers[i][100], &layers[i][101], &layers[i][102]);
	}
	fclose(preset);
	return 0;
}

unsigned char parse_arg(char *arg) {
	if(strc(arg, "--width=", 8)) {
		//width modified by user
		int nwid = -1;
		if(strlen(arg) == 8) return 1;
		nwid = atoi(&arg[8]);
		if(nwid < 10 || nwid > 1000000) {
			fprintf(stderr, "The user-specified width is not a valid value. The width will be determined by the preset.\n");
			return 0;
		}
		++width_redefines;
		fprintf(stderr, "Width was ");
		for(register int i = 0; i < width_redefines; ++i) fprintf(stderr, "re");
		fprintf(stderr, "defined manually. The new width is %i\n", nwid);
		width = nwid;
		return 0;
	} else if(strc(arg, "--height=", 9)) {
		//height modified by user
		int nhid = -1;
		if(strlen(arg) == 9) return 1;
		nhid = atoi(&arg[9]);
		if(nhid < 10 || nhid > 1000000) {
			fprintf(stderr, "The user-specified height is not a valid value. The height will be determined by the preset.\n");
			return 0;
		}
		++height_redefines;
		fprintf(stderr, "Height was ");
		register int i;
		for(i = 0; i < height_redefines; ++i) fprintf(stderr, "re");
		fprintf(stderr, "defined manually. The new height is %i\n", nhid);
		height = nhid;
		for(i = 0; i < no_of_layers; ++i) {
			if(max_height[i] > height - 4) max_height[i] = height - 4;
		}
		return 0;
	} else if(strc(arg, "--output=", 9)) {
		//output file name modified by user
		if(strlen(arg) == 9) return 1;
		output_filename = &arg[9];
		++output_redefines;
		fprintf(stderr, "Output filename ");
		for(register int i = 0; i < output_redefines; ++i) fprintf(stderr, "re");
		fprintf(stderr, "named manually. The new name is %s\n", &arg[9]);
		return 0;
	} else return 1;
}

unsigned char check_33(int X, int Y, int layer) {
	return tilemap[layer][X][Y] == '1' && tilemap[layer][X + 1][Y] == '1' && tilemap[layer][X + 2][Y] == '1' && tilemap[layer][X][Y + 1] == '1' && tilemap[layer][X + 1][Y + 1] == '1' && tilemap[layer][X + 2][Y + 1] == '1' && tilemap[layer][X][Y + 2] == '1' && tilemap[layer][X + 1][Y + 2] == '1' && tilemap[layer][X + 2][Y + 2] == '1';
}

int tile_code(char in, int layer, int X, int Y) {
	switch(in) {
		case '-':                          //top
			if(!centre_22[layer]) return layers[layer][0];
			else if(!(X % 2)) return layers[layer][70];
			else return layers[layer][71];
		case '<':                          //left edge
			if(!centre_22[layer]) return layers[layer][1];
			else if(!(Y % 2)) return layers[layer][72];
			else return layers[layer][74];
		case '>':                          //right edge
			if(!centre_22[layer]) return layers[layer][2];
			else if(!(Y % 2)) return layers[layer][73];
			else return layers[layer][75];
		case '!': return layers[layer][3]; //top left corner
		case '=': return layers[layer][4]; //top right corner
		case 'l': return layers[layer][5]; //left convex corner
		case 'L': return layers[layer][6]; //right convex corner
		case 'F': 
			if(!(random() % 2)) return layers[layer][7]; //decoration
			else return layers[layer][8];	//alt decoration
		case '1':                          //solid middle
			if(!centre_22[layer]) return layers[layer][9];
			else if(!(X % 2) && !(Y % 2)) return layers[layer][66];
			else if((X % 2) && !(Y % 2)) return layers[layer][67];
			else if(!(X % 2) && (Y % 2)) return layers[layer][68];
			else return layers[layer][69];
		case 'T': return layers[layer][11]; //single top
		case '|': return layers[layer][12]; //single middle
		case '/': return layers[layer][13]; //single bottom left
		case '\\': return layers[layer][14]; //single bottom right
		case '_': return layers[layer][15]; //single bidirectional
		case 'q': return layers[layer][16]; //top wrap
		case 'w': return layers[layer][17];
		case 'e': 
			if(!centre_22[layer]) return layers[layer][18];
			else if(!(X % 2)) return layers[layer][78];
			else return layers[layer][79];
		case 'r': return layers[layer][19];
		case 't': return layers[layer][20];
		case 'y':                           //middle wrap
			if(!centre_22[layer]) return layers[layer][21];
			else if(!(Y % 2)) return layers[layer][80];
			else return layers[layer][82];
		case 'u': 
			if(!centre_22[layer]) return layers[layer][22];
			else if(!(Y % 2)) return layers[layer][81];
			else return layers[layer][83];
		case 'P': return layers[layer][62]; //top side wrap
		case 'A': return layers[layer][63];
		case 'i': return layers[layer][23]; //bottom wrap
		case 'o': return layers[layer][24];
		case 'p': 
			if(!centre_22[layer]) return layers[layer][25];
			else if(!(X % 2)) return layers[layer][84];
			else return layers[layer][85];
		case 'a': return layers[layer][26];
		case 's': return layers[layer][27];
		case 'd': return layers[layer][28]; //column top wrap
		case 'f': return layers[layer][29];
		case 'g': return layers[layer][30];
		case 'h': return layers[layer][31]; //column middle wrap
		case 'j': return layers[layer][32];
		case 'S': return layers[layer][64]; //column top side wrap
		case 'D': return layers[layer][65];
		case 'k': return layers[layer][33]; //column bottom wrap
		case 'z': return layers[layer][34];
		case 'x': return layers[layer][35];
		case 'c': return layers[layer][36]; //regular concave smoothing
		case 'v': return layers[layer][37];
		case 'b': return layers[layer][38]; //left and right corner wrap
		case 'n': return layers[layer][39];
		case 'm': return layers[layer][40]; //column concave smoothing
		case ';': return layers[layer][41];
		case '*': return layers[layer][49];
		case '+': return layers[layer][42]; //left and right column wrap
		case '@': return layers[layer][43];
		case '#': return layers[layer][44]; //regular shallow smoothing
		case '$': return layers[layer][45];
		case '%': return layers[layer][46]; //column shallow smoothing
		case '^': return layers[layer][47];
		case '&': return layers[layer][48];
		case '(': return layers[layer][50]; //2x2 decoration
		case ')': return layers[layer][51];
		case '[': return layers[layer][52];
		case ']': return layers[layer][53];
		case 'Q': return layers[layer][54]; //sprinkle
		case 'W': return layers[layer][55];
		case 'E': 
			if(!centre_22[layer]) return layers[layer][56];
			else if(!(X % 2)) return layers[layer][86];
			else return layers[layer][87];
		case 'R': return layers[layer][57];
		case 'O': return layers[layer][58];
		case 'Y': return layers[layer][59]; //column sprinkle
		case 'U': return layers[layer][60];
		case 'I': return layers[layer][61];
		case '7': return layers[layer][88]; //3x3 decoration
		case '8': return layers[layer][89];
		case '9': return layers[layer][90];
		case '4': return layers[layer][91];
		case '5': return layers[layer][92];
		case '6': return layers[layer][93];
		case 'G': return layers[layer][94];
		case '2': return layers[layer][95];
		case '3': return layers[layer][96];
		case 'H': return layers[layer][97]; //smooth corner wrap
		case 'J': return layers[layer][98];
		case 'K': return layers[layer][99]; //smooth column corner wrap
		case 'Z': return layers[layer][100];
		case 'X': return layers[layer][101];//wrap shallow
		case 'C': return layers[layer][102];
		default: return layers[layer][10];  //air
	}
}

int main(int argc, char **argv) {

	register int i, j, k;

	printf("BackgroundGen: A SuperTux Parallax Background & Tilemap Generator\nOriginally created by Narre in July 2020\n\nv%s\nCopyleft 🄯 2020-2021 Jiří Paleček\nThis program comes with ABSOLUTELY NO WARRANTY.\nThis is free software, and you are welcome to redistribute it\nunder certain conditions. See the LICENCE file for more details.\n\n", VERSION);
	#ifdef MIDVERSION_PRESET
		printf("[WARNING] This version uses a mid-version preset. It is not compatible with any full release version and likely with no other development version. Do not use presets from different versions of BackgroundGen with this version even if the preset version is the same!!!\n\n");
	#endif

	if(argc < 2) {
		if(parse("default.bgp")) return 400;
	} else if(parse(argv[1])) return 400;

	if(width < 10 || height < 10 || width > 1000000 || height > 1000000) {
		fprintf(stderr, "Tilemap dimensions out of range. Terminating...\n");
		return 100;
	}
	if(width_factor > 1000000 || height_factor > 2 * height || decoration_factor < 1 || decoration_factor > 1000000 || width_factor < 1 || height_factor < 1) {
		fprintf(stderr, "Terrain generation constants out of range. Terminating...\n");
		return 200;
	} 
	if(!no_of_layers) {
		fprintf(stderr, "No layers are specified. Terminating...\n");
		return 300;
	}
	if(argc > 2) {
		for(i = 2; i < argc; ++i) {
			if(parse_arg(argv[i])) fprintf(stderr, "Argument \"%s\" cannot be parsed. Ignoring...\n", argv[i]);
		}
	}

	tilemap = (char ***)malloc(no_of_layers * sizeof(char **));
	for(i = 0; i < no_of_layers; ++i) {
		tilemap[i] = (char **)malloc(width * sizeof(char *));
		for(j = 0; j < width; ++j) {
			tilemap[i][j] = (char *)malloc(height * sizeof(char));
		}
	}

	for(k = 0; k < no_of_layers; ++k) {
		for(i = 0; i < width; ++i) {
			for(j = 0; j < height; ++j) {
				tilemap[k][i][j] = '0';
			}
		}
	}

	int position_x, position_y;
	unsigned char jump;
	int y_jump;
	int current_layer = 0;

	do {
		position_x = 0;
		position_y = (unsigned int)(height - (max_height[current_layer] / 2));
	
		srandom(clock());
	
		printf("[%s] Generating terrain...\n", layer_name[current_layer]);

		jump = 3;

		do {
			for(i = position_x; i < position_x + jump && i < width; ++i) {
				for(j = position_y; j < height; ++j) {
					tilemap[current_layer][i][j] = '1';
				}
			}
		
			position_x += jump;
			jump = (unsigned char)(2 + random() % width_factor);
			y_jump = (random() % (2 * height_factor + 1)) - height_factor;
			if(disable_shallow && y_jump) {
				if(y_jump == 1) ++y_jump;
				else if(y_jump == -1) --y_jump;
			}
			if(y_jump > 0 && position_y + y_jump > (height - 1)) y_jump = -y_jump;
			else if(y_jump < 0 && position_y + y_jump < (height - max_height[current_layer])) y_jump = -y_jump;
			position_y += y_jump;
			
			if(position_y < 2) position_y -= y_jump;
			if(position_y >= height - 1) position_y -= y_jump;

			jump = (unsigned char)(2 + random() % width_factor);
			if(disable_single && jump < 3) jump = 4;
			if(position_x + jump > width - 4) jump += 5;
		} while(position_x < width);

		if(hole_frequency_factor) {
			printf("[%s] Carving terrain...\n", layer_name[current_layer]);
			int current_hole_end;
			for(i = 5; i < (width - max_hole_width - 5); ++i) {
				if(random() % hole_frequency_factor == 1) {
					current_hole_end = i + ((random() % (max_hole_width - 1)) + 1);
					if(current_hole_end < i + min_hole_width) current_hole_end = i + min_hole_width;
					for(; i <= current_hole_end; ++i) {
						for(j = 0; j < height; ++j) {
							tilemap[current_layer][i][j] = '0';
						}
					}
					if(disable_single) {
						int nx = i - 1;
						int ny = height - 1;
						while(tilemap[current_layer][nx][ny] == '0') --nx;
						while(tilemap[current_layer][nx][ny] == '1') --ny;
						if(tilemap[current_layer][nx - 1][ny] == '1' || tilemap[current_layer][nx - 1][ny + 1] == '0') {
							for(k = ny; k < height; ++k) tilemap[current_layer][nx][k] = '0';
						}
						ny = height - 1;
						++nx;
						while(tilemap[current_layer][nx][ny] == '0') ++nx;
						while(tilemap[current_layer][nx][ny] == '1') --ny;
						if(tilemap[current_layer][nx + 1][ny] == '1' || tilemap[current_layer][nx + 1][ny + 1] == '0') {
							for(k = ny; k < height; ++k) tilemap[current_layer][nx][k] = '0';
						}
					}
				}
			}
		}

		printf("[%s] Brushing terrain...\n", layer_name[current_layer]);
	
		position_x = 0;
		position_y = 0;
	
		while(tilemap[current_layer][0][position_y] == '0') ++position_y;
		tilemap[current_layer][0][position_y] = '-';
		tilemap[current_layer][1][position_y] = '-';
		--position_y;
		++position_x;
	
		while(position_x < width - 2) {
			if(position_y == height - 1) {
				if(tilemap[current_layer][position_x + 1][position_y] == '0') ++position_x;
				else --position_y;
			} else {
				if(tilemap[current_layer][position_x][position_y + 1] == '0' && tilemap[current_layer][position_x + 1][position_y + 1] == '0') ++position_y;
				else if(tilemap[current_layer][position_x + 1][position_y] == '0') ++position_x;
				else --position_y;
			}

			if(position_y == height - 1) {
				if(tilemap[current_layer][position_x - 1][position_y] != '0') tilemap[current_layer][position_x - 1][position_y] = '>';
				else if(tilemap[current_layer][position_x + 1][position_y] != '0') tilemap[current_layer][position_x + 1][position_y] = '<';
				if(tilemap[current_layer][position_x - 2][position_y] == '0' && tilemap[current_layer][position_x - 1][position_y] != '0')
					tilemap[current_layer][position_x - 1][position_y] = '|';

				continue;
			}
	
			if(/*-*/   tilemap[current_layer][position_x - 1][position_y] == '0'
				&& tilemap[current_layer][position_x + 1][position_y] == '0'
				&& tilemap[current_layer][position_x - 1][position_y + 1] != '0'
				&& tilemap[current_layer][position_x][position_y + 1] != '0'
				&& tilemap[current_layer][position_x + 1][position_y + 1] != '0'
			  ) tilemap[current_layer][position_x][position_y + 1] = '-';
			else if (/*<*/	tilemap[current_layer][position_x][position_y - 1] == '0'
				&&	tilemap[current_layer][position_x][position_y + 1] == '0'
				&&	tilemap[current_layer][position_x + 1][position_y - 1] != '0'
				&&	tilemap[current_layer][position_x + 1][position_y] != '0'
				&&	tilemap[current_layer][position_x + 1][position_y + 1] != '0'
			  ) tilemap[current_layer][position_x + 1][position_y] = '<';
			else if (/*>*/	tilemap[current_layer][position_x][position_y - 1] == '0'
				&&	tilemap[current_layer][position_x][position_y + 1] == '0'
				&&	tilemap[current_layer][position_x - 1][position_y - 1] != '0'
				&&	tilemap[current_layer][position_x - 1][position_y] != '0'
				&&	tilemap[current_layer][position_x - 1][position_y + 1] != '0'
			  ) tilemap[current_layer][position_x - 1][position_y] = '>';
			else if (/*!*/ 	tilemap[current_layer][position_x + 1][position_y + 1] != '0'
				&&	tilemap[current_layer][position_x][position_y + 1] == '0'
				&&	tilemap[current_layer][position_x + 1][position_y] == '0'
			  ) tilemap[current_layer][position_x + 1][position_y + 1] = '!';
			else if (/*=*/	tilemap[current_layer][position_x - 1][position_y + 1] != '0'
				&&	tilemap[current_layer][position_x - 1][position_y] == '0'
				&&	tilemap[current_layer][position_x][position_y + 1] == '0'
			  ) tilemap[current_layer][position_x - 1][position_y + 1] = '=';
			else if (/*l*/	tilemap[current_layer][position_x + 1][position_y] != '0'
				&&	tilemap[current_layer][position_x + 1][position_y + 1] != '0'
				&&	tilemap[current_layer][position_x][position_y + 1] != '0'
			  ) { tilemap[current_layer][position_x + 1][position_y] = '<';
			      if(tilemap[current_layer][position_x - 1][position_y + 1] != '0') tilemap[current_layer][position_x][position_y + 1] = '-';
				else tilemap[current_layer][position_x][position_y + 1] = '!';
			      tilemap[current_layer][position_x + 1][position_y + 1] = 'l';
			    }
			else if (/*L*/	tilemap[current_layer][position_x - 1][position_y] != '0'
				&&	tilemap[current_layer][position_x - 1][position_y + 1] != '0'
				&&	tilemap[current_layer][position_x][position_y + 1] != '0'
			  ) { if(tilemap[current_layer][position_x - 1][position_y] != '=')tilemap[current_layer][position_x - 1][position_y] = '>';
			      if(tilemap[current_layer][position_x + 1][position_y + 1] != 0) tilemap[current_layer][position_x][position_y + 1] = '-';
				else tilemap[current_layer][position_x][position_y + 1] = '=';
			      tilemap[current_layer][position_x - 1][position_y + 1] = 'L';
			    }
			if(position_x == 1) continue;
			if (/*T*/	tilemap[current_layer][position_x - 2][position_y] == '0'
				&&	tilemap[current_layer][position_x - 1][position_y - 1] == '0'
				&&	tilemap[current_layer][position_x - 1][position_y] != '0'
			  ) {
				tilemap[current_layer][position_x - 1][position_y] = 'T';
				if(tilemap[current_layer][position_x][position_y + 1] != '0') {
					if(tilemap[current_layer][position_x - 2][position_y + 1] != '0')
						tilemap[current_layer][position_x - 1][position_y + 1] = '_';
					else tilemap[current_layer][position_x - 1][position_y + 1] = '\\';
				}
			    }
			else if (/*|*/	tilemap[current_layer][position_x - 2][position_y] == '0'
				&&	tilemap[current_layer][position_x - 1][position_y] != '0'
			  ) {
				tilemap[current_layer][position_x - 1][position_y] = '|';
				if(   tilemap[current_layer][position_x - 2][position_y + 1] == '0'
				   && (tilemap[current_layer][position_x][position_y + 1] == '-'
				   || tilemap[current_layer][position_x][position_y + 1] == '=')
				   && tilemap[current_layer][position_x - 1][position_y + 1] != '0'
					) tilemap[current_layer][position_x - 1][position_y + 1] = '\\';
				else if (  tilemap[current_layer][position_x - 2][position_y + 1] == '-'
					&& tilemap[current_layer][position_x][position_y + 1] == '-'
					) tilemap[current_layer][position_x - 1][position_y + 1] = '_';
			    }
			else if (   (tilemap[current_layer][position_x - 2][position_y] == '-'
				 ||  tilemap[current_layer][position_x - 2][position_y] == '!')
				 && tilemap[current_layer][position_x - 1][position_y] == '>'
				) tilemap[current_layer][position_x - 1][position_y] = '/';
		}

		position_x = width - 1;
		position_y = 0;
		while(tilemap[current_layer][position_x][position_y] == '0') ++position_y;
		tilemap[current_layer][position_x][position_y] = '-';

		position_x = 1;
		position_y = 0;

		direction from;
	
		if(wrap[current_layer]) {
			printf("[%s] Smoothing terrain...\n", layer_name[current_layer]);
			while(tilemap[current_layer][0][position_y] == '0') ++position_y;
			tilemap[current_layer][0][--position_y] = 'e';
			tilemap[current_layer][1][position_y] = 'e';
		}
		while(wrap[current_layer] && position_x < width - 2) {
			if(position_y == height - 1) {
				if(tilemap[current_layer][position_x + 1][position_y] == '0') {
					++position_x;
					from = LEFT;
				} else {
					--position_y;
					from = BOTTOM;
				}
			} else {
				if(tilemap[current_layer][position_x][position_y + 1] == '0' && tilemap[current_layer][position_x + 1][position_y + 1] == '0') {
					++position_y;
					from = TOP;
				} else if(tilemap[current_layer][position_x + 1][position_y] == '0') {
					++position_x;
					from = LEFT;
				} else {
					--position_y;
					from = BOTTOM;
				}
			}
			switch(from) {
				case LEFT:
					//pointer came from a hole
					if(LY == height - 1 && (tilemap[current_layer][LX - 1][LY] == '0' || tilemap[current_layer][LX - 1][LY] == 'u'))
						continue;
					else if(LY == height - 1)
						tilemap[current_layer][LX][LY] = 'u';
					//pointer came from a flat middle
					else if(tilemap[current_layer][LX][LY + 1] == '-') {
						tilemap[current_layer][LX][LY] = 'e';
						if(wrap[current_layer] > (unsigned char)1 && (tilemap[current_layer][LX - 1][LY] == '>' || tilemap[current_layer][LX - 1][LY] == '|'))
							tilemap[current_layer][LX][LY] = 'n';
						else if(wrap[current_layer] > (unsigned char)1 && (tilemap[current_layer][LX - 1][LY] == '=' || tilemap[current_layer][LX - 1][LY] == 'T'))
							tilemap[current_layer][LX][LY] = 'C';
					}
					//pointer came from a flat leftside
					else if(tilemap[current_layer][LX][LY + 1] == '!')
						tilemap[current_layer][LX][LY] = 'w';
					//pointer came from a top-left corner
					else if(tilemap[current_layer][LX + 1][LY + 1] == '!')
						tilemap[current_layer][LX][LY] = 'q';
					//pointer came from a top-left column corner
					else if(tilemap[current_layer][LX + 1][LY + 1] == 'T')
						tilemap[current_layer][LX][LY] = 'd';
					//pointer came from a flat rightside
					else if(tilemap[current_layer][LX][LY + 1] == '=') {
						tilemap[current_layer][LX][LY] = 'r';
						if(wrap[current_layer] > (unsigned char)1 && tilemap[current_layer][LX - 1][LY] == '>')
							tilemap[current_layer][LX][LY] = 'n';
						else if(wrap[current_layer] > (unsigned char)1 && tilemap[current_layer][LX - 1][LY] == '|')
							tilemap[current_layer][LX][LY] = '@';
						else if(wrap[current_layer] > (unsigned char)2 && tilemap[current_layer][LX - 1][LY] == '=')
							tilemap[current_layer][LX][LY] = 'C';
						else if(wrap[current_layer] > (unsigned char)2 && tilemap[current_layer][LX - 1][LY] == 'T')
							tilemap[current_layer][LX][LY] = 'C';
					}
					//pointer came from a column-top
					else if(tilemap[current_layer][LX][LY + 1] == 'T')
						tilemap[current_layer][LX][LY] = 'f';
					//pointer came from a right corner
					else if(tilemap[current_layer][LX - 1][LY] == '>')
						tilemap[current_layer][LX][LY] = 'n';
					//pointer came from a right column corner
					else if(tilemap[current_layer][LX - 1][LY] == '|' || tilemap[current_layer][LX - 1][LY] == 'T') 
						tilemap[current_layer][LX][LY] = '@';
					break;
				case TOP:
					//pointer came from a flat side
					if(tilemap[current_layer][TX - 1][TY] == '>')
						tilemap[current_layer][TX][TY] = 'u';
					//pointer came from a flat topside
					else if(tilemap[current_layer][TX - 1][TY] == '=')
						tilemap[current_layer][TX][TY] = 'A';
					//pointer came from a top-right corner
					else if(tilemap[current_layer][TX - 1][TY + 1] == '=')
						tilemap[current_layer][TX][TY] = 't';
					//pointer came from a top-right column corner
					else if(tilemap[current_layer][TX - 1][TY + 1] == 'T')
						tilemap[current_layer][TX][TY] = 'g';
					//pointer came from a flat column side
					else if(tilemap[current_layer][TX - 1][TY] == '|')
						tilemap[current_layer][TX][TY] = 'j';
					//pointer came from a flat columnn top corner
					else if(tilemap[current_layer][TX - 1][TY] == 'T')
						tilemap[current_layer][TX][TY] = 'D';
					else if(tilemap[current_layer][TX - 1][TY] == '/')
						tilemap[current_layer][TX][TY] = 'j';
					break;
				case BOTTOM:
					if(BY < (height - 1) && tilemap[current_layer][BX][BY + 1] == '!') {
						tilemap[current_layer][BX][BY] = 'w';
						if(wrap[current_layer] > (unsigned char)1 && tilemap[current_layer][BX + 1][BY] == '<')
							tilemap[current_layer][BX][BY] = 'b';
						else if(wrap[current_layer] > (unsigned char)1 && tilemap[current_layer][BX + 1][BY] == '|')
							tilemap[current_layer][BX][BY] = '+';
						else if(wrap[current_layer] > (unsigned char)2 && (tilemap[current_layer][BX + 1][BY] == '!' || tilemap[current_layer][BX + 1][BY] == 'T'))
							tilemap[current_layer][BX][BY] = 'X';
					}
					else if(BY < (height - 1) && tilemap[current_layer][BX][BY + 1] == 'b') {
						if(tilemap[current_layer][BX + 1][BY] == '<') tilemap[current_layer][BX][BY] = 'y';
						else tilemap[current_layer][BX][BY] = 'P';
					} else if(tilemap[current_layer][BX + 1][BY] == '<' && (BY == (height - 1) || (BY < (height - 1) && (tilemap[current_layer][BX][BY + 1] == '0' || tilemap[current_layer][BX][BY + 1] == 'y' || tilemap[current_layer][BX][BY + 1] == 'b'))))
						tilemap[current_layer][BX][BY] = 'y';
					//pointer came from a flat topside
					else if(tilemap[current_layer][BX + 1][BY] == '!' && tilemap[current_layer][BX][BY + 1] != '-' && tilemap[current_layer][BX][BY + 1] != '!')
						tilemap[current_layer][BX][BY] = 'S';
					//pointer came from a left corner
					else if(BY < (height - 1) && (tilemap[current_layer][BX][BY + 1] == '-' || tilemap[current_layer][BX][BY + 1] == '!') && (tilemap[current_layer][BX + 1][BY] == '<' || tilemap[current_layer][BX + 1][BY] == '!' || tilemap[current_layer][BX + 1][BY] == 'T')) {
						if(wrap[current_layer] < (unsigned char)3) tilemap[current_layer][BX][BY] = 'b';
						else {
							if(tilemap[current_layer][BX + 1][BY] == '!' || tilemap[current_layer][BX + 1][BY] == 'T') tilemap[current_layer][BX][BY] = 'X';
							else tilemap[current_layer][BX][BY] = 'b';
						}
					}
					//pointer came from a left column corner
					else if(BY < (height - 1) && tilemap[current_layer][BX + 1][BY] == '|' && (tilemap[current_layer][BX][BY + 1] == '-' || tilemap[current_layer][BX][BY + 1] == '!'))
						tilemap[current_layer][BX][BY] = '+';
					//pointer came from a flat column side
					else if(tilemap[current_layer][BX + 1][BY] == '|' || tilemap[current_layer][BX + 1][BY] == '\\')
						tilemap[current_layer][BX][BY] = 'h';
					//pointer came from a top left column flatside
					else if(tilemap[current_layer][BX + 1][BY] == 'T') {
						tilemap[current_layer][BX][BY] = 'S';
						if(wrap[current_layer] > (unsigned char)2 && (tilemap[current_layer][BX][BY + 1] == '-' || tilemap[current_layer][BX][BY + 1] == '!'))
							tilemap[current_layer][BX][BY] = 'X';
					}
					break;
				default: break;
			}
		}

		if(wrap[current_layer]) for(i = 1; i < width - 2; ++i) for(j = 1; j < height; ++j) {
			if(tilemap[current_layer][i][j] == 'L') {
				if(tilemap[current_layer][i][j - 1] == '>') tilemap[current_layer][i][j - 1] = 'v';
				else if(tilemap[current_layer][i][j - 1] == '=') tilemap[current_layer][i][j - 1] = '$';
				if(wrap[current_layer] > (unsigned char)2) tilemap[current_layer][i + 1][j] = 'J';
			} else if(tilemap[current_layer][i][j] == 'l') {
				if(tilemap[current_layer][i][j - 1] == '<') tilemap[current_layer][i][j - 1] = 'c';
				else if(tilemap[current_layer][i][j - 1] == '!') tilemap[current_layer][i][j - 1] = '#';
				if(wrap[current_layer] > (unsigned char)2) tilemap[current_layer][i - 1][j] = 'H';
			} else if(tilemap[current_layer][i][j] == '_') {
				if(tilemap[current_layer][i][j - 1] == 'T') tilemap[current_layer][i][j - 1] = '&';
				else if(tilemap[current_layer][i][j - 1] == '|') tilemap[current_layer][i][j - 1] = '*';				
				if(wrap[current_layer] > (unsigned char)2) {
					tilemap[current_layer][i - 1][j] = 'K';
					tilemap[current_layer][i + 1][j] = 'Z';
				}
			} else if(tilemap[current_layer][i][j] == '\\') {
				if(tilemap[current_layer][i][j - 1] == '|') tilemap[current_layer][i][j - 1] = ';';
				else if(tilemap[current_layer][i][j - 1] == 'T') tilemap[current_layer][i][j - 1] = '^';
				if(wrap[current_layer] > (unsigned char)2) tilemap[current_layer][i + 1][j] = 'Z';
			} else if(tilemap[current_layer][i][j] == '/') {
				if(tilemap[current_layer][i][j - 1] == '|') tilemap[current_layer][i][j - 1] = 'm';
				else if(tilemap[current_layer][i][j - 1] == 'T') tilemap[current_layer][i][j - 1] = '%';
				if(wrap[current_layer] > (unsigned char)2) tilemap[current_layer][i - 1][j] = 'K';
			}
		}

		if(wrap[current_layer]) {
			position_x = width - 2;
			position_y = 0;
			while(tilemap[current_layer][position_x][position_y + 1] == '0') ++position_y;
			tilemap[current_layer][position_x][position_y] = 'e';
			tilemap[current_layer][position_x + 1][position_y] = 'e';
		}

		if(wrap[current_layer] > 1) {
			printf("[%s] Sprinkling terrain...\n", layer_name[current_layer]);
			for(i = 0; i < width; ++i) {
				for(j = 1; j < height - 1; ++j) {
					if(tilemap[current_layer][i][j] == 'q') {
						tilemap[current_layer][i][j - 1] = 'Q';
						break;
					} else if (tilemap[current_layer][i][j] == 'w') {
						tilemap[current_layer][i][j - 1] = 'W';
						break;
					} else if (tilemap[current_layer][i][j] == 'e') {
						tilemap[current_layer][i][j - 1] = 'E';
						break;
					} else if (tilemap[current_layer][i][j] == 'r') {
						tilemap[current_layer][i][j - 1] = 'R';
						break;
					} else if (tilemap[current_layer][i][j] == 't') {
						tilemap[current_layer][i][j - 1] = 'O';
						break;
					} else if (tilemap[current_layer][i][j] == 'd') {
						tilemap[current_layer][i][j - 1] = 'Y';
						break;
					} else if (tilemap[current_layer][i][j] == 'f') {
						tilemap[current_layer][i][j - 1] = 'U';
						break;
					} else if (tilemap[current_layer][i][j] == 'g') {
						tilemap[current_layer][i][j - 1] = 'I';
						break;
					}
				}
			}
		}

		printf("[%s] Decorating terrain...\n", layer_name[current_layer]);
		unsigned long ran;
		for(i = 0; i < width; ++i) for(j = 0; j < height; ++j) {
			ran = random();
			if(tilemap[current_layer][i][j] == '1' && !(ran % decoration_factor)) {
				if(((!decor22[current_layer]) && (!decor33[current_layer])) || !(ran % 2)) tilemap[current_layer][i][j] = 'F';
				else if(   i < (width - 3) && j < (height - 3)
					&& decor33[current_layer] && !decor22[current_layer]
					&& check_33(i, j, current_layer)
				) {
					tilemap[current_layer][i][j] = '7'; tilemap[current_layer][i + 1][j] = '8'; tilemap[current_layer][i + 2][j] = '9';
					tilemap[current_layer][i][j + 1] = '4'; tilemap[current_layer][i + 1][j + 1] = '5'; tilemap[current_layer][i + 2][j + 1] = '6';
					tilemap[current_layer][i][j + 2] = 'G'; tilemap[current_layer][i + 1][j + 2] = '2'; tilemap[current_layer][i + 2][j + 2] = '3';
				} else if( decor22[current_layer] && decor33[current_layer]) {
					if(!(random() % 2)) {
						if(i < (width - 3) && j < (height - 3) && check_33(i, j, current_layer)) {
							tilemap[current_layer][i][j] = '7'; tilemap[current_layer][i + 1][j] = '8'; tilemap[current_layer][i + 2][j] = '9';
							tilemap[current_layer][i][j + 1] = '4'; tilemap[current_layer][i + 1][j + 1] = '5'; tilemap[current_layer][i + 2][j + 1] = '6';
							tilemap[current_layer][i][j + 2] = 'G'; tilemap[current_layer][i + 1][j + 2] = '2'; tilemap[current_layer][i + 2][j + 2] = '3';
						} else tilemap[current_layer][i][j] = 'F';
					} else {
						if(i < (width - 2) && j < (height - 2) && tilemap[current_layer][i + 1][j] == '1' && tilemap[current_layer][i + 1][j + 1] == '1' && tilemap[current_layer][i][j + 1] == '1') {
							tilemap[current_layer][i][j] = '(';
							tilemap[current_layer][i + 1][j] = ')';
							tilemap[current_layer][i][j + 1] = '[';
							tilemap[current_layer][i + 1][j + 1] = ']';
						} else tilemap[current_layer][i][j] = 'F';
					}
				} else if(   i < (width - 2) && j < (height - 2)
					&& decor22[current_layer]
					&& tilemap[current_layer][i + 1][j] == '1'
					&& tilemap[current_layer][i + 1][j + 1] == '1'
					&& tilemap[current_layer][i][j + 1] == '1'
				) {
					tilemap[current_layer][i][j] = '(';
					tilemap[current_layer][i + 1][j] = ')';
					tilemap[current_layer][i][j + 1] = '[';
					tilemap[current_layer][i + 1][j + 1] = ']';
				} else tilemap[current_layer][i][j] = 'F';
			}
		}

	} while(++current_layer < no_of_layers);

	if(!output_redefines) printf("Saving as background.stb\n");
	else {
		if(strlen((const char *)output_filename) > 4 && strc(&output_filename[strlen((const char *)output_filename) - 4], ".stl", 4)) {
			save_as_level = 1;
			printf("Saving as %s\n", output_filename);
		} else if(strlen((const char *)output_filename) < 4 || !strc(&output_filename[strlen((const char *)output_filename) - 4], ".stb", 4)) {
			fnout = 1;
			nout = strmrg(output_filename, ".stb");
			printf("Saving as %s\n", nout);
		} else printf("Saving as %s\n", output_filename);
	}

	FILE *out;
	if(!output_redefines) out = fopen("background.stb", "w");
	else if(!fnout) out = fopen(output_filename, "w");
	else {
		out = fopen(nout, "w");
		free(nout);
	}

	if(save_as_level) {
		output_filename[strlen((const char *)output_filename) - 4] = '\0';
		fprintf(out, "(supertux-level\n\t(version 3)\n\t(name (_ \"%s\"))\n\t(author \"%s\")\n\t(license \"CC-BY-SA 4.0 International\")\n", output_filename, author);
		fprintf(out, "\t(sector\n\t\t(name \"main\")\n\t\t(ambient-light\n\t\t\t(color 1 1 1)\n\t\t)\n\t\t(gradient\n\t\t\t(top_color 0.3 0.4 0.75)\n\t\t\t(bottom_color 1 1 1)\n\t\t)\n\t\t(camera\n\t\t\t(mode \"normal\")\n\t\t)\n\t\t(spawnpoint\n\t\t\t(name \"main\")\n\t\t\t(x 32)\n\t\t\t(y %i)\n\t\t)\n", ((height / 2) - 2) * 32);

	}

	current_layer = 0;
	do {
		fprintf(out, "\t\t(tilemap\n\t\t\t(z-pos %i)\n\t\t\t(solid #%c)\n\t\t\t(width %i)\n\t\t\t(height %i)\n\t\t\t(name \"%s\")\n\t\t\t(speed-x %f)\n\t\t\t(speed-y %f)\n\t\t\t(tiles\n\t\t\t\t", layer_zpos[current_layer], is_solid(current_layer), width, height, layer_name[current_layer], layer_speed[current_layer], layer_speed[current_layer]);
	
		for(i = 0; i < height; ++i) {
			#ifdef NO_REPLACE_ID
				for(j = 0; j < width; ++j) fprintf(out, "%c ", tilemap[current_layer][j][i]);
			#else
				for(j = 0; j < width; ++j) fprintf(out, "%i ", tile_code(tilemap[current_layer][j][i], current_layer, j, i));
			#endif
			if(i != height - 1)fprintf(out, "\n\t\t\t\t");
		}
		fprintf(out, "\n\t\t\t)\n\t\t)\n");

	} while(++current_layer < no_of_layers);

	if(save_as_level) fprintf(out, "\t)\n)");

	printf("Terminating.\n");

	for(k = 0; k < no_of_layers; ++k) {
		for(i = 0; i < width; ++i) free(tilemap[k][i]);
		free(tilemap[k]);
	}
	for(i = 0; i < no_of_layers; ++i) free(layers[i]);
	free(layers);
	free(layer_speed);
	for(i = 0; i < no_of_layers; ++i) free(layer_name[i]);
	free(layer_name);
	free(layer_zpos);
	free(tilemap);
	free(solid);
	free(wrap);
	free(max_height);
	free(decor22);
	free(decor33);
	free(flip);
	free(centre_22);
	free(author);
	fclose(out);
	return 0;
}
