# BackgroundGen

A SuperTux tile parallax background generator

Compile with gcc (`gcc main.c -o bggen`) or use the bundled Makefile (`make`)

**Usage:**

`./bggen` - generate a background using default.bgp\
`./bggen PRESET` - generate a background using PRESET\
`./bggen PRESET [arguments]` - generate a background based on PRESET (you can use default.bgp) modified by arguments

_Possible arguments are:_\
`--width=NUM` - modifies the width of the generated tilemaps\
`--height=NUM` - modifies the height of the generated tilemaps\
`--output=NAME` - changes the name of the output file to NAME (default is background.stb, *.stl will result in the generated tilemaps being saved as a plain SuperTux level)
