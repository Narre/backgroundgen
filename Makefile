#  BackgroundGen
#  Copyleft 🄯 2020-2021 Jiří Paleček <narre@protonmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public Licence as published by
#  the Free Software Foundation, either version 3 of the Licence, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public Licence for more details.
#
#  You should have received a copy of the GNU General Public Licence
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

CC=gcc
CFLAGS=-Wall -Wextra -Werror -std=gnu17

all: bggen

bggen.o: main.c version.h
	$(CC) $(CFLAGS) main.c -c -o bggen.o

bggen: bggen.o
	$(CC) $(CFLAGS) bggen.o -o bggen

clean:
	rm -f bggen.o bggen

install: bggen
	cp bggen /usr/bin/
